-----------------------------------------
XENATX - Version alpha 0.9.2-ML
-----------------------------------------

FR - INFORMATION
----------------
Le XENATX est une application autonome de la marque XENATON (c). 
Cette application utilisable dans un simple navigateur récent - sans besoin d'être connecté à Internet - fait partie d'une suite d'outils, avec ou sans services en ligne associés, facilitant l'utilisation du chiffrement asymétrique, symétrique, OTP et des signatures.

ATTENTION, le XENATX fonctionne surtout avec les trois systèmes d'exploitation Windows, MacOS, Linux. Il y a des restrictions d'usage avec iOS sur iphone, comme l'impossibilité de jouer du son (morse). Il faut également dans certains cas utiliser une application tierce qui intègre une liseuse "html". Le navigateur par défaut type Safari ou même Firefox ne peut pas forcément faire fonctionner directement le XENATX correctement.
Pour Android, le fonctionnement est un peu similaire à celui pour l'iphone.

COMMENT L'UTILISER ?
C'est très simple. Une fois le dossier dézippé, cherchez le fichiez index.html dans le dossier au même niveau hiérarchique que le présent fichier README.txt et double-cliquez dessus. Votre navigateur par défaut va s'ouvrir et le XENATX est alors prêt à l'emploi, sans besoin de connexion Internet.

EMPREINTE - HASH
Vous pouvez obtenir l'empreinte SHA-512 du dossier zippé de cette version du XENATX sur gitlab (https://gitlab.com/xenaton/xenatx/-/wikis/version), sur Telegram (t.me/xenaton_official) et sur le site xenaton.com

LICENCE
La licence a été maintenue "propriétaire" mais l'usage est gratuit et sans limitation. Dans le XENATX, seuls les éléments de design, d'ergonomie et d'implémentation du code sont réellement soumis à notre droit de propriété intellectuelle.
En tant qu'application js et html uniquement, le code est entièrement ouvert et volontairement non obfusqué pour faciliter les contrôles de sécurité et d'intégrité.
Plus d'explications dans la rubrique Outils et les deux onglets "Sécurité du code" et "XENATON".

BIBLIOTHEQUES OPEN SOURCE
Le XENATX comprend de nombreuses bibliothèques open source. Vous pouvez vérifier les empreintes de ces bibliothèques en utilisant les sources publiques indiquées ci-dessous des versions utilisées (également indiquées sur la page principale index.html - Section "Outils" et onglet "Sécurité du code")
Merci également de consulter leur licence respective.

GARANTIE
Aucune garantie logicielle. L'Utilisateur reconnaît et accepte que l'utilisation du Logiciel XENATX est à ses risques et périls. Le Logiciel et la documentation connexe sont fournis "TELS QUELS" sans aucune garantie d'aucune sorte et XENATON DÉCLINE EXPRESSÉMENT TOUTE GARANTIE, EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES IMPLICITES DE QUALITÉ MARCHANDE ET D'ADÉQUATION À UN USAGE PARTICULIER.

Pour toute demande ou suggestion, contactez-nous via xenaton.com

Merci de votre intérêt.

---

EN - INFORMATION
----------------
The XENATX is an autonomous app of brand XENATON (c).
This app usable in browser - without the need to be connected to the Internet - is part of a suite of tools, with or without associated online services, to facilitate the use of asymmetric, symmetric, OTP encryption and signatures.

ATTENTION, the XENATX mainly works with the three operating systems Windows, MacOS, Linux. There are usage restrictions with iOS on iphone, such as the impossibility of playing sound (morse code). In some cases, you also have to use a third-party application that includes an "html" reader. The default browser such as Safari or even Firefox cannot necessarily make the XENATX work correctly directly.
For Android, the operation is somewhat similar to that for the iphone.

HOW TO USE IT?
It's very simple. Once you unzip the folder, find the index.html file in the folder at the same hierarchical level as this README.txt file and double-click it. Your default browser will open and the XENATX is then ready to use, without needing an Internet connection.

HASH
You can get the SHA-512 hash of the zipped folder of this version of XENATX on gitlab (https://gitlab.com/xenaton/xenatx/-/wikis/version), on Telegram (t.me/xenaton_official) and on xenaton.com website

LICENCE
The license has been maintained as "proprietary" for now but the use is free and without limitation. In the XENATX, only the elements of design, ergonomics and implementation of the code are really subject to our intellectual property rights.
As a js and html only application, the code is fully open and intentionally unobfuscated to facilitate security and integrity checks.
More explanations in the Tools section and the two tabs "Security code" and "XENATON".

OPEN SOURCE LIBRARIES
The XENATX includes many open source libraries. Please verify checksum by using the indicated public sources regarding the versions used further below.
Please also have a look to their licence.

WARRANTY
No Software Warranty. Any user acknowledges and agrees that the use of the Software XENATX is at User’s sole risk. The Software and related documentation are provided “AS IS” and without any warranty of any kind and XENATON EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

For any request or suggestion contact us through xenaton.com

Thanks for reading and interest.

-------------------------
BIBLIOTHEQUES - LIBRARIES
-------------------------
Audio-decoder-adaptive - Décodage morse
Source publique&nbsp;: https://morsecode.world/js/audio-decoder-adaptive.96da976088f5fb7c78e537b303a0ebee.js 
Si la chaine suivante entre crochets du lien ci-dessus change [ 96da976088f5fb7c78e537b303a0ebee ].js, copiez et collez juste le contenu https://morsecode.world/js/audio-decoder-adaptive.*.js et collez-le localement en remplacement. 
Les différences ne devraient pas être grandes et vous êtes ainsi garantis d'utiliser une source publique non "manipulée". La bibliothèque complète est ici, pour étude : https://github.com/scp93ch/morse-pro
Appel en pied de page du fichier index.html : assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js

Bootstrap
Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/bootstrap.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/bootstrap-5.2.0.min.js

Chroma
Source publique&nbsp;: https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/chroma-2.4.2.min.js

Clipboard
Source publique&nbsp;: https://raw.githubusercontent.com/zenorocha/clipboard.js/master/dist/clipboard.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/clipboard-2.0.10.min.js

Crypto-js
Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/crypto-js-4.1.1.min.js

Forge
Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/forge-1.3.1.min.js

i18next
Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/i18next-21.8.14.min.js

jQuery
Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/jquery-2.2.4.min.js

Js-cookie
Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)
Appel dans le head du fichier index.html : assets/javascripts/libs/js-cookie-2.1.4.min.js

Jscwlib - Encodage morse - Morse encoding
FR - ATTENTION ! Quelques changement avec cette bilbliothèque voir plus bas.
EN - ATTENTION! - Some changes with this library see below.
Source publique&nbsp;: https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/
Appel dans le head du fichier index.html&nbsp;: assets/javascripts/libs/jscwlib.js

Modernizr
Source publique&nbsp;: https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)
Appel dans le head du fichier index.html : assets/javascripts/libs/modernizr-3.6.0.min.js

MosaicVisualHash - Empreinte visuelle - Visual hash
Source publique&nbsp;: https://raw.githubusercontent.com/jfietkau/Mosaic-Visual-Hash/master/mosaicVisualHash.js
Appel dans le head du fichier index.html&nbsp;: assets/javascripts/libs/mosaicVisualHash-1.0.1.js

Openpgp
Source publique&nbsp;: https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/openpgpjs-5.3.1.min.js

Qrcode
Source publique&nbsp;: https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js
Appel dans le head du fichier index.html : assets/javascripts/libs/qrcode.min.js


--------------------------------------------------
FR - Modification de cette bibliothèque jscwlib.js
--------------------------------------------------
jscwlib.js - Encodage morse - fkurz.net - assets/javascripts/libs/jscwlib.js - https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/)
ATTENTION ! - Très important de remettre les valeurs par défaut avant de faire le calcul et le contrôle des empreintes SHA-512. Voir les modifications ci-dessous.

    Ligne 194
    La fréquence a été mise à 563 à la place de 600.

    Ligne 207
    this.cgiurl = "https://cgi2.lcwo.net/cgi-bin/";
    Suppression de l’url
    this.cgiurl = "";

    Ligne 310
    this.oscillator.frequency.setValueAtTime(600, this.audioCtx.currentTime); // value in hertz
    La valeur a été mise 563
    this.oscillator.frequency.setValueAtTime(563, this.audioCtx.currentTime); // value in hertz

    Ligne 553
    ctx.fillStyle = 'rgb(200, 200, 200)';
    Modification de la couleur :
    ctx.fillStyle = 'rgb(234, 236, 239)’;

    Ligne 540
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    Modification de la couleur :
    ctx.strokeStyle = 'rgb(11, 110, 253)’;

    Ligne 1287
    freq_label.innerHTML = "600 Hz";
    La valeur a été mise à 563
    freq_label.innerHTML = « 563 Hz";

---------------------------------------------
EN - Changing of this library jscwlib.js
---------------------------------------------
jscwlib.js - Encodage morse - fkurz.net - assets/javascripts/libs/jscwlib.js - https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/)
WARNING ! - Very important to set back the default values before hashing with SHA-512 and compare fingerprints. Have a look to the modified values below.

    Line 194
    Frequency value has been set to 563 instead of 600.

    Line 207
    this.cgiurl = "https://cgi2.lcwo.net/cgi-bin/";
    Deletion of the url
    this.cgiurl = "";

    Line 310
    this.oscillator.frequency.setValueAtTime(600, this.audioCtx.currentTime); // value in hertz
    Value has been set to 563
    this.oscillator.frequency.setValueAtTime(563, this.audioCtx.currentTime); // value in hertz

    Line 553
    ctx.fillStyle = 'rgb(200, 200, 200)';
    Changing the color:
    ctx.fillStyle = 'rgb(234, 236, 239)’;

    Line 540
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    Changing the color:
    ctx.strokeStyle = 'rgb(11, 110, 253)’;

    Line 1287
    freq_label.innerHTML = "600 Hz";
    Value has been set to 563
    freq_label.innerHTML = « 563 Hz";