// ******
// MORSE
// ******
(function() {

    "use strict";

    // MORSE CONVERSION
    const alphabet = {
        "A": {
            "code": "•−",
            "spoken": "DIT-DAH",
            "audio": "assets/sounds/morse/A_morse_code.ogg",
            "key": "65"
        },
        "B": {
            "code": "-•••",
            "spoken": "DAH-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/B_morse_code.ogg",
            "key": "66"
        },
        "C": {
            "code": "−•−•",
            "spoken": "DAH-DIT-DAH-DIT",
            "audio": "assets/sounds/morse/C_morse_code.ogg",
            "key": "67"
        },
        "D": {
            "code": "−••",
            "spoken": "DAH-DIT-DIT",
            "audio": "assets/sounds/morse/D_morse_code.ogg",
            "key": "68"
        },
        "E": {
            "code": "•",
            "spoken": "DIT",
            "audio": "assets/sounds/morse/E_morse_code.ogg",
            "key": "69"
        },
        "F": {
            "code": "••−•",
            "spoken": "DIT-DIT-DAH-DIT",
            "audio": "assets/sounds/morse/F_morse_code.ogg",
            "key": "70"
        },
        "G": {
            "code": "−−•",
            "spoken": "DAH-DAH-DIT",
            "audio": "assets/sounds/morse/G_morse_code.ogg",
            "key": "71"
        },
        "H": {
            "code": "••••",
            "spoken": "DIT-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/H_morse_code.ogg",
            "key": "72"
        },
        "I": {
            "code": "••",
            "spoken": "DIT-DIT",
            "audio": "assets/sounds/morse/I_morse_code.ogg",
            "key": "73"
        },
        "J": {
            "code": "•−−−",
            "spoken": "DIT-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/J_morse_code.ogg",
            "key": "74"
        },
        "K": {
            "code": "−•−",
            "spoken": "DAH-DIT-DAH",
            "audio": "assets/sounds/morse/K_morse_code.ogg",
            "key": "75"
        },
        "L": {
            "code": "•−••",
            "spoken": "DIT-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/L_morse_code.ogg",
            "key": "76"
        },
        "M": {
            "code": "−−",
            "spoken": "DAH-DAH",
            "audio": "assets/sounds/morse/M_morse_code.ogg",
            "key": "77"
        },
        "N": {
            "code": "−•",
            "spoken": "DAH-DIT",
            "audio": "assets/sounds/morse/N_morse_code.ogg",
            "key": "78"
        },
        "O": {
            "code": "−−−",
            "spoken": "DAH-DAH-DAH",
            "audio": "assets/sounds/morse/O_morse_code.ogg",
            "key": "79"
        },
        "P": {
            "code": "•−•−",
            "spoken": "DIT-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/P_morse_code.ogg",
            "key": "80"
        },
        "Q": {
            "code": "−−•−",
            "spoken": "DAH-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/Q_morse_code.ogg",
            "key": "81"
        },
        "R": {
            "code": "•−•",
            "spoken": "DIT-DAH-DIT",
            "audio": "assets/sounds/morse/R_morse_code.ogg",
            "key": "82"
        },
        "S": {
            "code": "•••",
            "spoken": "DIT-DIT-DIT",
            "audio": "assets/sounds/morse/S_morse_code.ogg",
            "key": "83"
        },
        "T": {
            "code": "−",
            "spoken": "DAH",
            "audio": "assets/sounds/morse/T_morse_code.ogg",
            "key": "84"
        },
        "U": {
            "code": "••−",
            "spoken": "DIT-DIT-DAH",
            "audio": "assets/sounds/morse/U_morse_code.ogg",
            "key": "85"
        },
        "V": {
            "code": "•••−",
            "spoken": "DIT-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/V_morse_code.ogg",
            "key": "86"
        },
        "W": {
            "code": "•−−",
            "spoken": "DIT-DAH-DAH",
            "audio": "assets/sounds/morse/W_morse_code.ogg",
            "key": "87"
        },
        "X": {
            "code": "−••−",
            "spoken": "DAH-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/X_morse_code.ogg",
            "key": "88"
        },
        "Y": {
            "code": "−•−−",
            "spoken": "DAH-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/Y_morse_code.ogg",
            "key": "89"
        },
        "Z": {
            "code": "−−••",
            "spoken": "DAH-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/Z_morse_code.ogg",
            "key": "90"
        }
    };

    const numbers = {
        "0": {
            "code": "−−−−−",
            "spoken": "DAH-DAH-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/0_number_morse_code.ogg",
            "key": "48"
        },
        "1": {
            "code": "•−−−−",
            "spoken": "DIT-DAH-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/1_number_morse_code.ogg",
            "key": "49"
        },
        "2": {
            "code": "••−−−",
            "spoken": "DIT-DIT-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/2_number_morse_code.ogg",
            "key": "50"
        },
        "3": {
            "code": "•••−−",
            "spoken": "DIT-DIT-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/3_number_morse_code.ogg",
            "key": "51"
        },
        "4": {
            "code": "••••−",
            "spoken": "DIT-DIT-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/4_number_morse_code.ogg",
            "key": "52"
        },
        "5": {
            "code": "•••••",
            "spoken": "DIT-DIT-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/5_number_morse_code.ogg",
            "key": "53"
        },
        "6": {
            "code": "−••••",
            "spoken": "DAH-DIT-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/6_number_morse_code.ogg",
            "key": "54"
        },
        "7": {
            "code": "−−•••",
            "spoken": "DAH-DAH-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/7_number_morse_code.ogg",
            "key": "55"
        },
        "8": {
            "code": "−−−••",
            "spoken": "DAH-DAH-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/8_number_morse_code.ogg",
            "key": "56"
        },
        "9": {
            "code": "−−−−•",
            "spoken": "DAH-DAH-DAH-DAH-DIT",
            "audio": "assets/sounds/morse/9_number_morse_code.ogg",
            "key": "57"
        }
        };

        const punctuation = {
        ".": {
            "code": "•−•−•−",
            "spoken": "DIT-DAH-DIT-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/period_morse_code.ogg",
            "key": "190"
        },
        ",": {
            "code": "−−••−−",
            "spoken": "DAH-DAH-DIT-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/comma_morse_code.ogg",
            "key": "188"
        },
        "?": {
            "code": "••−−••",
            "spoken": "DIT-DIT-DAH-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/question_morse_code.ogg",
            "key": "63"
        },
        "!": {
            "code": "−•−•−−",
            "spoken": "DAH-DIT-DAH-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/exclamation_morse_code.ogg",
            "key": "33"
        },
        "-": {
            "code": "−••••−",
            "spoken": "DAH-DIT-DIT-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/dash_morse_code.ogg",
            "key": "45"
        },
        "/": {
            "code": "−••−•",
            "spoken": "DAH-DIT-DIT-DAH-DIT",
            "audio": "assets/sounds/morse/slash_morse_code.ogg",
            "key": "191"
        },
        "@": {
            "code": "•−−•−•",
            "spoken": "DIT-DAH-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/at_morse_code.ogg",
            "key": "64"
        },
        "(": {
            "code": "−•−−•",
            "spoken": "DAH-DIT-DAH-DAH-DIT",
            "audio": "assets/sounds/morse/openParens_morse_code.ogg",
            "key": "40"
        },
        ")": {
            "code": "−•−−•−",
            "spoken": "DAH-DIT-DAH-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/closeParens_morse_code.ogg",
            "key": "41"
        }
        };

        const alphaNumPlus = {
        "A": {
            "code": "•−",
            "spoken": "DIT-DAH",
            "audio": "assets/sounds/morse/A_morse_code.ogg",
            "key": "65"
        },
        "B": {
            "code": "-•••",
            "spoken": "DAH-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/B_morse_code.ogg",
            "key": "66"
        },
        "C": {
            "code": "−•−•",
            "spoken": "DAH-DIT-DAH-DIT",
            "audio": "assets/sounds/morse/C_morse_code.ogg",
            "key": "67"
        },
        "D": {
            "code": "−••",
            "spoken": "DAH-DIT-DIT",
            "audio": "assets/sounds/morse/D_morse_code.ogg",
            "key": "68"
        },
        "E": {
            "code": "•",
            "spoken": "DIT",
            "audio": "assets/sounds/morse/E_morse_code.ogg",
            "key": "69"
        },
        "F": {
            "code": "••−•",
            "spoken": "DIT-DIT-DAH-DIT",
            "audio": "assets/sounds/morse/F_morse_code.ogg",
            "key": "70"
        },
        "G": {
            "code": "−−•",
            "spoken": "DAH-DAH-DIT",
            "audio": "assets/sounds/morse/G_morse_code.ogg",
            "key": "71"
        },
        "H": {
            "code": "••••",
            "spoken": "DIT-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/H_morse_code.ogg",
            "key": "72"
        },
        "I": {
            "code": "••",
            "spoken": "DIT-DIT",
            "audio": "assets/sounds/morse/I_morse_code.ogg",
            "key": "73"
        },
        "J": {
            "code": "•−−−",
            "spoken": "DIT-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/J_morse_code.ogg",
            "key": "74"
        },
        "K": {
            "code": "−•−",
            "spoken": "DAH-DIT-DAH",
            "audio": "assets/sounds/morse/K_morse_code.ogg",
            "key": "75"
        },
        "L": {
            "code": "•−••",
            "spoken": "DIT-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/L_morse_code.ogg",
            "key": "76"
        },
        "M": {
            "code": "−−",
            "spoken": "DAH-DAH",
            "audio": "assets/sounds/morse/M_morse_code.ogg",
            "key": "77"
        },
        "N": {
            "code": "−•",
            "spoken": "DAH-DIT",
            "audio": "assets/sounds/morse/N_morse_code.ogg",
            "key": "78"
        },
        "O": {
            "code": "−−−",
            "spoken": "DAH-DAH-DAH",
            "audio": "assets/sounds/morse/O_morse_code.ogg",
            "key": "79"
        },
        "P": {
            "code": "•−•−",
            "spoken": "DIT-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/P_morse_code.ogg",
            "key": "80"
        },
        "Q": {
            "code": "−−•−",
            "spoken": "DAH-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/Q_morse_code.ogg",
            "key": "81"
        },
        "R": {
            "code": "•−•",
            "spoken": "DIT-DAH-DIT",
            "audio": "assets/sounds/morse/R_morse_code.ogg",
            "key": "82"
        },
        "S": {
            "code": "•••",
            "spoken": "DIT-DIT-DIT",
            "audio": "assets/sounds/morse/S_morse_code.ogg",
            "key": "83"
        },
        "T": {
            "code": "−",
            "spoken": "DAH",
            "audio": "assets/sounds/morse/T_morse_code.ogg",
            "key": "84"
        },
        "U": {
            "code": "••−",
            "spoken": "DIT-DIT-DAH",
            "audio": "assets/sounds/morse/U_morse_code.ogg",
            "key": "85"
        },
        "V": {
            "code": "•••−",
            "spoken": "DIT-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/V_morse_code.ogg",
            "key": "86"
        },
        "W": {
            "code": "•−−",
            "spoken": "DIT-DAH-DAH",
            "audio": "assets/sounds/morse/W_morse_code.ogg",
            "key": "87"
        },
        "X": {
            "code": "−••−",
            "spoken": "DAH-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/X_morse_code.ogg",
            "key": "88"
        },
        "Y": {
            "code": "−•−−",
            "spoken": "DAH-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/Y_morse_code.ogg",
            "key": "89"
        },
        "Z": {
            "code": "−−••",
            "spoken": "DAH-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/Z_morse_code.ogg",
            "key": "90"
        },
        "0": {
            "code": "−−−−−",
            "spoken": "DAH-DAH-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/0_number_morse_code.ogg",
            "key": "48"
        },
        "1": {
            "code": "•−−−−",
            "spoken": "DIT-DAH-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/1_number_morse_code.ogg",
            "key": "49"
        },
        "2": {
            "code": "••−−−",
            "spoken": "DIT-DIT-DAH-DAH-DAH",
            "audio": "assets/sounds/morse/2_number_morse_code.ogg",
            "key": "50"
        },
        "3": {
            "code": "•••−−",
            "spoken": "DIT-DIT-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/3_number_morse_code.ogg",
            "key": "51"
        },
        "4": {
            "code": "••••−",
            "spoken": "DIT-DIT-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/4_number_morse_code.ogg",
            "key": "52"
        },
        "5": {
            "code": "•••••",
            "spoken": "DIT-DIT-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/5_number_morse_code.ogg",
            "key": "53"
        },
        "6": {
            "code": "−••••",
            "spoken": "DAH-DIT-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/6_number_morse_code.ogg",
            "key": "54"
        },
        "7": {
            "code": "−−•••",
            "spoken": "DAH-DAH-DIT-DIT-DIT",
            "audio": "assets/sounds/morse/7_number_morse_code.ogg",
            "key": "55"
        },
        "8": {
            "code": "−−−••",
            "spoken": "DAH-DAH-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/8_number_morse_code.ogg",
            "key": "56"
        },
        "9": {
            "code": "−−−−•",
            "spoken": "DAH-DAH-DAH-DAH-DIT",
            "audio": "assets/sounds/morse/9_number_morse_code.ogg",
            "key": "57"
        },
        ".": {
            "code": "•−•−•−",
            "spoken": "DIT-DAH-DIT-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/period_morse_code.ogg",
            "key": "190"
        },
        ",": {
            "code": "−−••−−",
            "spoken": "DAH-DAH-DIT-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/comma_morse_code.ogg",
            "key": "188"
        },
        "?": {
            "code": "••−−••",
            "spoken": "DIT-DIT-DAH-DAH-DIT-DIT",
            "audio": "assets/sounds/morse/question_morse_code.ogg",
            "key": "63"
        },
        "!": {
            "code": "−•−•−−",
            "spoken": "DAH-DIT-DAH-DIT-DAH-DAH",
            "audio": "assets/sounds/morse/exclamation_morse_code.ogg",
            "key": "33"
        },
        "-": {
            "code": "−••••−",
            "spoken": "DAH-DIT-DIT-DIT-DIT-DAH",
            "audio": "assets/sounds/morse/dash_morse_code.ogg",
            "key": "45"
        },
        "/": {
            "code": "−••−•",
            "spoken": "DAH-DIT-DIT-DAH-DIT",
            "audio": "assets/sounds/morse/slash_morse_code.ogg",
            "key": "191"
        },
        "@": {
            "code": "•−−•−•",
            "spoken": "DIT-DAH-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/at_morse_code.ogg",
            "key": "64"
        },
        "(": {
            "code": "−•−−•",
            "spoken": "DAH-DIT-DAH-DAH-DIT",
            "audio": "assets/sounds/morse/openParens_morse_code.ogg",
            "key": "40"
        },
        ")": {
            "code": "−•−−•−",
            "spoken": "DAH-DIT-DAH-DAH-DIT-DAH",
            "audio": "assets/sounds/morse/closeParens_morse_code.ogg",
            "key": "41"
        }
    };

    // TEXT TO MORSE (DISPLAY)
    //************************

    function textToMorse(value) {
        let userInput = value;
        let userInputArray = userInput.split(' ');
        let codedOutput = "";

        for (let word of userInputArray) {
            word = word.replace(/[^0-9a-z.,/.,?!()@-]/gi, '');
            for (let character of word) {
                if (!isNaN(character)) {
                    let charObject = numbers[character];
                    codedOutput = codedOutput + charObject.code + ' ';
                }
                else if (character.toUpperCase() in alphabet) {
                    let charObject = alphabet[character.toUpperCase()];
                    codedOutput = codedOutput + charObject.code + ' ';
                }
                else {
                    let charObject = punctuation[character];
                    codedOutput = codedOutput + charObject.code + ' ';
                }
            }
            codedOutput = codedOutput + ' | ';
        }
        const codeText = document.getElementById("code-text"); // Display under button when press "View morse"
        codeText.innerHTML = codedOutput.slice(0, -2);
    }


    // CONVERT FROM STR TO HEX
    //************************
    
    var btnConvertToHex = document.getElementById('btnConvertToHex');
    if(btnConvertToHex){
        btnConvertToHex.addEventListener('click', function(e){
            e.preventDefault();

            var errorMorse = document.getElementById("errorMorse");
            errorMorse.innerHTML = '';

            var cypherTextToMorse = document.getElementById('cypherTextToMorse').value;
            if('' == cypherTextToMorse) {                           
                document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textToMorseEmpty') + '</p>'
                return;
            }

            document.getElementById('cypherTextEncoded').value = xenStrToHex(cypherTextToMorse).toUpperCase();        
            
            // clear input
            const str = document.getElementById("cypherTextToMorse");
            str.value = '';
        });
    }

    // CONVERT FROM HEX TO STR
    //************************
    var btnConvertHexToUtf8 = document.getElementById('btnConvertHexToUtf8');
    if(btnConvertHexToUtf8){
        btnConvertHexToUtf8.addEventListener('click', function(e){
            e.preventDefault();

            var errorMorse = document.getElementById("errorMorse");
            errorMorse.innerHTML = '';

            var cypherTextEncoded = document.getElementById('cypherTextEncoded').value;
            if('' == cypherTextEncoded) {                           
                document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textOrHexaEmpty') + '</p>'
                return;
            }
            document.getElementById('cypherTextToMorse').value = xenHexToStr(cypherTextEncoded);
            // clear
            const str = document.getElementById("cypherTextEncoded");
            str.value = '';

        });
    }

    // VOICE MESSAGES
    //***************
    var voiceMessages = { 0 : 'transmit_message_in_3_sec_fr', 1 : 'transmit_hash_sha_256_in_3_sec_fr' };
    var audios = {};
    var index = 0;
    Object.values(voiceMessages).forEach(voiceMessage => {
        var audio = new Audio();
        audio.src = `assets/sounds/morse/voices/${voiceMessages[index]}.mp3`; // load the sound file
        audios[voiceMessage] = audio;
        index++;
    });

    // FUNCTION TO PLAY/TRANSMIT MORSE (SOUND)
    //****************************************
    function transmit(cypherText, voiceMessage) {

        var m = {};

        function jscwtPlay(cypherText) {
            m = new jscw();
            m.play(cypherText);
            
            var length = m.getLength().toFixed(1);
            var intervalID = window.setInterval(function() {
                if (!m) {
                    window.clearInterval(intervalID)
                    rem.innerHTML = `- / ${length}`;
                    return;
                }
                var time = m.getTime().toFixed(1);
                rem.innerHTML = `${time} / ${length}`;
                if (m.getRemaining()) {
                    m.oscilloscope(document.getElementById('osci'));
                } else {
                   window.clearInterval(intervalID)
                }
            }, 100);
            
        }

        const playStartVoicePromise = new Promise(function(resolve, reject) {
            // start of transmission voice message
            var audio = audios[voiceMessage];
            audio.addEventListener('ended', resolve);
            audio.play();
        });

        playStartVoicePromise
            .then(playMorse, playMorseError);

        function playMorse() {
            setTimeout(function(){
                jscwtPlay(cypherText);
            }, 3000);
        }

        function playMorseError() {
            document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.transmissionProblem') + '</p>'
            return;
        }
    }

    // PLAY MORSE (SOUND)
    //*******************
    function computedHashSha256(el) {
        return CryptoJS.SHA256(el).toString().toUpperCase();
    }
    
    var btnTextToMorse = document.getElementById('btnTextToMorse');
    if(btnTextToMorse){
        btnTextToMorse.addEventListener('click', function(e){
            e.preventDefault();

            var errorMorse = document.getElementById("errorMorse");
            errorMorse.innerHTML = '';

            var cypherTextToMorse = document.getElementById('cypherTextToMorse').value;
            var cypherTextEncoded = document.getElementById('cypherTextEncoded').value;

            if(cypherTextToMorse != '') {
                if(!isAlphaNumUpper(cypherTextToMorse)) {                                                   
                    document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textUpperOnlyEmpty') + '</p>'
                    return;
                }
                // https://developer.mozilla.org/en-US/docs/Web/API/AudioScheduledSourceNode/ended_event
                transmit(cypherTextToMorse, voiceMessages[0]);
                document.getElementById("hashTextToMorse").value = computedHashSha256(cypherTextToMorse);
            }
            else if (cypherTextEncoded != ''){
                transmit(cypherTextEncoded, voiceMessages[0]);
                document.getElementById("hashTextToMorse").value = computedHashSha256(cypherTextEncoded);
            }
            else {
                document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textCipheredEmpty') + '</p>'
                return;
            }
        });
    } 

    // DISPLAY TEXT MORSE ONLY
    //************************
    var btnTextOnly = document.getElementById('btnTextOnly');
    if(btnTextOnly){
        btnTextOnly.addEventListener('click', function(e){
            e.preventDefault();
            var cypherTextToMorse = document.getElementById('cypherTextToMorse').value;
            var cypherTextEncoded = document.getElementById('cypherTextEncoded').value;

            if(cypherTextToMorse != '') {
                window.scrollBy(0, 300);
                textToMorse(cypherTextToMorse);
            }
            else if (cypherTextEncoded != ''){
                window.scrollBy(0, 300);
                textToMorse(cypherTextEncoded);
            }
            else {
                document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textCipheredEmpty') + '</p>'
            }
        });
    }

    // STOP
    //******
    // DO a real web api stop button one day, this is just a reload button so no possibility to go on after a temporary stop.
    function reload() {
        location.reload(true); // the "true" option means cache will be removed: what we need.
    }  
        
    window.onload = function(){
        document.getElementById('btnStop').addEventListener('click', reload, false);
    };


    // HASH - TAM - TX
    //****************
    var btnTransmitHashTextToMorse = document.getElementById('btnTransmitHashTextToMorse');
    if(btnTransmitHashTextToMorse){
        btnTransmitHashTextToMorse.addEventListener('click', function(e){
            e.preventDefault();

            var errorMorse = document.getElementById("errorMorse");
            errorMorse.innerHTML = '';

            var hashTextToMorse = document.getElementById('hashTextToMorse').value;
            if(hashTextToMorse != '') {
                var rem = document.getElementById('rem');
                rem.innerHTML = '';
                transmit(hashTextToMorse, voiceMessages[1]);
            }
            else {
                document.getElementById("errorMorse").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.transmitMessageBeforeHash') + '</p>'
                return;
            }
        });
    } 

    // HASH - TAM - RX
    //****************
    function checkComputedHashMorseToText() {
        // Compare and manage succes or error
        var hashComputedMorseToText = document.getElementById("hashComputedMorseToText").value;
        var hashSentMorseToText = document.getElementById("hashSentMorseToText").value;
        
        if(hashComputedMorseToText != '' && hashSentMorseToText != '') {
            if (hashComputedMorseToText.toString().toLowerCase() === hashSentMorseToText.toString().toLowerCase()) {
                var status = 'success';
                var text = '<i class="bi bi-file-check"></i> ' + i18next.t('various.hashOK');
            } else {
                var status = 'danger';
                var text = '<i class="bi bi-exclamation-triangle"></i> ' + i18next.t('error.hashKO');
            }
            document.getElementById("errorHashMorseToText").innerHTML = '<div class="alert alert-' + status + '">' + text + '</div>';
        } else {
            document.getElementById("errorHashMorseToText").innerHTML = '<div class="alert alert-danger">' + i18next.t('various.indicateHashToCompare') + '</div>';
        }
    }

    var btnCheckHashSentAndComputed = document.getElementById('btnCheckHashSentAndComputed');
    if(btnCheckHashSentAndComputed){
        btnCheckHashSentAndComputed.addEventListener('click', function(e){
            e.preventDefault();

            var errorHashMorseToText = document.getElementById("errorHashMorseToText");
            errorHashMorseToText.innerHTML = '';

            checkComputedHashMorseToText();
        });
    }

})();
