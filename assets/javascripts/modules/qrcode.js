// *****************
// GENERATE QR CODE
// *****************

// ****
// EMIT
// ****
(function() {

    "use strict";

    // qrcode character counter
    var displayEnteredCharQRC = document.getElementById("displayEnteredCharQRC");  
    var variousContent = document.getElementById("variousContent");   
    const qrcodeCharCounter = () => {
        let numOfEnteredChars = variousContent.value.length;
        displayEnteredCharQRC.textContent = numOfEnteredChars;
    };
    variousContent.addEventListener("input", qrcodeCharCounter);

    // multiples QR Codes counter
    function addNewQrCode(index, item, multiple) {
        var qrcodeWrapper = document.getElementById('qrcodeWrapper');
        var hash = CryptoJS.SHA1(item).toString().toUpperCase();

        var newDivTitle = document.createElement('p');
        var newDivSubTitle = document.createElement('p');
        newDivTitle.setAttribute("class", "h3");
        newDivTitle.innerText = 'QR Code n°' + (index+1);
        newDivSubTitle.innerText = separatorEveryNChar(hash, ' ', 4) + ' ' + i18next.t('tools.qrcode.hashParenthesis');
        qrcodeWrapper.appendChild(newDivTitle);
        qrcodeWrapper.appendChild(newDivSubTitle);

        // visual hash
        var visualHashCanvas = document.createElement('canvas');
        visualHashCanvas.setAttribute("id", "hashCanvas" + index);
        visualHashCanvas.setAttribute("class", "qrcode__pad");
        visualHashCanvas.setAttribute("title", i18next.t('tools.qrcode.displayQrCode.visualHashSubtitle') + separatorEveryNChar(hash, ' ', 4));
        qrcodeWrapper.appendChild(visualHashCanvas);
        
        var canvas = document.getElementById("hashCanvas" + index);
        visualHash(canvas, hash);

        if(multiple) {
            var newDivQRCode = document.createElement('div');
            var newHr = document.createElement('hr');
            newDivQRCode.setAttribute("id", "qrcode" + index);
            newDivQRCode.setAttribute("class", "qrcode__pad");
            qrcodeWrapper.appendChild(newDivQRCode);
            qrcodeWrapper.appendChild(newHr);
        } 
    }
   
    // factory to generate QR Code
    function makeCode () {		
        // set transport capacity of QR Code
        var qrcodeCapacity = document.getElementById('qrcodeCapacity');
        if(qrcodeCapacity){
            var variousContentBlock = qrcodeCapacity.value;
        }

        var variousContent = document.getElementById('variousContent').value.trim();
        var variousContentMaxCharacters = variousContentBlock * 100;

        // error empty
        if(variousContent == '') {
            document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.contentToQRCodeEmpty') + '</p>'
            return;
        }

        // error too long for max number of QR Codes
        if(variousContent.length > variousContentMaxCharacters) {
            document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.tooBigForQRCode1') + variousContentMaxCharacters + i18next.t('error.tooBigForQRCode2') + '</p>';
            return;
        }
        
        var qrcodeWrapper = document.getElementById('qrcodeWrapper');

        if(variousContent.length > variousContentBlock) {

            function splitToSubstrings(str, nChar) {
                const arr = [];
                for (var i = 0; i < str.length; i += nChar) {
                    arr.push(str.slice(i, i + nChar));
                }
                return arr;
            }

            var blocks = splitToSubstrings(variousContent, Number(variousContentBlock));  

            // number of QR Codes
            var numberOfQRCodes = document.createElement('p');
            numberOfQRCodes.setAttribute("class", "h5");
            
            var numberOfBlocks = blocks.length;
            numberOfQRCodes.innerText = numberOfBlocks + i18next.t('various.manyQRCodesGenerated');
            qrcodeWrapper.appendChild(numberOfQRCodes);

            // global hash
            var hash = CryptoJS.SHA1(variousContent).toString().toUpperCase();
            var globalHashDiv = document.createElement('p');
            globalHashDiv.setAttribute("class", "h4");
            globalHashDiv.innerText = i18next.t('tools.qrcode.textualAndVisualHashSubtitle') + separatorEveryNChar(hash, ' ', 4);;
            qrcodeWrapper.appendChild(globalHashDiv);

            // global visual hash
            var visualHashCanvas = document.createElement('canvas');
            visualHashCanvas.setAttribute("id", "hashCanvas");
            visualHashCanvas.setAttribute("class", "qrcode__pad");
            visualHashCanvas.setAttribute("title", i18next.t('tools.qrcode.visualGlobalHashIndication'));
            qrcodeWrapper.appendChild(visualHashCanvas);
            
            var canvas = document.getElementById("hashCanvas");
            visualHash(canvas, hash, 256, 256);

            try {
                blocks.forEach((item, index) => {
                    addNewQrCode(index, item, true);
                    var qrcode = new QRCode("qrcode" + index, {
                        correctLevel : QRCode.CorrectLevel.L
                    });
                    qrcode.makeCode(item);
                });
            }
            catch (error) {
                if(error) {
                         
                    document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.convertToHexMultiQRC') + '</p>'

                    // display none
                    var doNotDisplay = document.getElementById('qrcodeWrapper');
                    doNotDisplay.style.display = 'none';
                    
                    return;
                }
            }

            // global hash at bottom
            var globalHash2 = document.createElement('p');
            globalHash2.setAttribute("class", "h4");
            globalHash2.innerText = i18next.t('tools.qrcode.textualAndVisualHashSubtitle') + separatorEveryNChar(hash, ' ', 4);;
            qrcodeWrapper.appendChild(globalHash2);

            // global visual hash at bottom
            var globalVisualHashCanvas2 = document.createElement('canvas');
            globalVisualHashCanvas2.setAttribute("id", "globalVisualHashCanvas2");
            globalVisualHashCanvas2.setAttribute("class", "qrcode__pad");
            globalVisualHashCanvas2.setAttribute("title", i18next.t('tools.qrcode.visualGlobalHashIndication'));
            qrcodeWrapper.appendChild(globalVisualHashCanvas2);

            var canvas2 = document.getElementById("globalVisualHashCanvas2");
            visualHash(canvas2, hash, 256, 256);
        } 
        else {
            // One QR Code
            var numberOfQRCodes = document.createElement('p');
            numberOfQRCodes.innerText = i18next.t('various.onlyOneQRCodeGenerated');
            qrcodeWrapper.appendChild(numberOfQRCodes);

            addNewQrCode(0, variousContent, false);
            var qrcode = new QRCode("qrcode", {
                correctLevel : QRCode.CorrectLevel.L
            });
            qrcode.makeCode(variousContent);
        }
    }

    // generate one or many QR Codes
    var generateQRCode = document.getElementById('generateQRCode');
    if(generateQRCode){
        generateQRCode.addEventListener('click', function(e){
            e.preventDefault();
            var errorQRCode = document.getElementById("errorQRCode");
            errorQRCode.innerHTML = '';

            const myNode = document.getElementById("qrcodeWrapper");
                  myNode.innerHTML = '';
            qrcodeWrapper.style.display = 'block';
            setDisabled('generateQRCode');
            removeDisabled('cancelQRCode');
            makeCode();
        })
    }

    // cancel
    var cancelQRCode = document.getElementById('cancelQRCode');
    if(cancelQRCode){
        cancelQRCode.addEventListener('click', function(e){
            e.preventDefault();
            var errorQRCode = document.getElementById("errorQRCode");
            errorQRCode.innerHTML = '';

            var variousContent = document.getElementById("variousContent");
                  variousContent.value = '';

            var nodeQrcode = document.getElementById("qrcode");
                  nodeQrcode.innerHTML = '';

            var nodeQrcodeWrapper = document.getElementById("qrcodeWrapper");
                  nodeQrcodeWrapper.innerHTML = '';

            qrcodeWrapper.style.display = 'block';

            var displayEnteredCharQRC = document.getElementById("displayEnteredCharQRC");  
            displayEnteredCharQRC.textContent = 0;

            removeDisabled('generateQRCode');
            setDisabled('cancelQRCode');
        })
    }
})(visualHash);


// *******
// RECEIVE
// *******
(function() {

    "use strict";
    
    setDisabled('cancelNumberOfQRCodesToRead');
    
    // generate X textarea to receive content of each QR Code
    document.getElementById("btnNumberOfQRCodesToRead").addEventListener('click', function(e) {
        var numberOfQRCodes = document.getElementById("numberOfQRCodesToRead").value;  
        var errorNumberOfQRCodesToRead = document.getElementById("errorNumberOfQRCodesToRead");  

        if(numberOfQRCodes == '' || !isNumber(numberOfQRCodes)) {
            errorNumberOfQRCodesToRead.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.numberOfQRCodesToReadEmpty') + '</p>'
            return;
        }
        createQRCodes(numberOfQRCodes);
        setDisabled('numberOfQRCodesToRead');
        removeDisabled('cancelNumberOfQRCodesToRead');
    });

    // cancel
    document.getElementById("cancelNumberOfQRCodesToRead").addEventListener('click', function(e) {
        var numberOfQRCodes = document.getElementById("numberOfQRCodesToRead");
        numberOfQRCodes.value = '';
        errorNumberOfQRCodesToRead.innerHTML = '';
        QRCReceiverWrapper.innerHTML = '';

        setDisabled('cancelNumberOfQRCodesToRead');
        removeDisabled('numberOfQRCodesToRead');      
        removeDisabled('btnNumberOfQRCodesToRead');    
    });

    // multiples QR Codes
    function createQRCodes(nb) {

        var QRCReceiverWrapper = document.getElementById('QRCReceiverWrapper');

        for (var i = 0; i < nb; i++) {
            var newDivTitle = document.createElement('p');
            var newDivHash = document.createElement('p');
            var visualHashCanvas = document.createElement('canvas');
            var newTextarea = document.createElement('textarea');
            var newError = document.createElement('p');
            var newButton = document.createElement('button');
            var newCancelButton = document.createElement('button');
            var newHr = document.createElement('br');

            newError.setAttribute("id", "errorHashQRC" + (i + 1));
            
            newButton.innerText = i18next.t('tools.qrcode.readQrCode.checkHashBtn');
            newButton.setAttribute("id", "buttonQRC" + (i + 1));
            newButton.setAttribute("class", "btn btn-sm btn-warning btn__qrc--receiver");
            
            newCancelButton.innerText = i18next.t('various.cancel');
            newCancelButton.setAttribute("id", "cancelButtonQRC" + (i + 1));
            newCancelButton.setAttribute("class", "btn btn-sm btn-outline-danger btn__qrc--receiver");
            
            newDivHash.setAttribute("id", "hashQRC" + (i + 1));
            
            newDivTitle.innerText = 'QR Code n°' + (i + 1);
            newDivTitle.setAttribute("class", "h3");

            // visual hash
            visualHashCanvas.setAttribute("id", "receiverHashCanvas" + (i + 1));
            visualHashCanvas.setAttribute("class", "mg5");
            visualHashCanvas.setAttribute("title", i18next.t('tools.qrcode.readQrCode.visualHashTitle'));

            newTextarea.setAttribute("id", "QRC" + (i + 1));
            newTextarea.setAttribute("class", "form-control");

            QRCReceiverWrapper.appendChild(newDivTitle);
            QRCReceiverWrapper.appendChild(newDivHash);
            QRCReceiverWrapper.appendChild(visualHashCanvas);
            QRCReceiverWrapper.appendChild(newTextarea);
            QRCReceiverWrapper.appendChild(newError);
            QRCReceiverWrapper.appendChild(newButton);
            QRCReceiverWrapper.appendChild(newCancelButton);
            QRCReceiverWrapper.appendChild(newHr);

            // display none visual hash
            visualHashCanvas.style.display = 'none';
            newError.style.display = 'none';

            document.getElementById("buttonQRC" + (i + 1))
            .addEventListener('click', function(e) {
                var textarea = this.previousElementSibling.previousElementSibling;
                var errorHash = this.previousElementSibling;
                errorHash.innerHTML = '';
                errorHash.style.display = 'none';

                if(textarea.value == '') {
                    errorHash.style.display = 'block';
                    errorHash.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.QRCodeEmpty') + '</p>'
                    return;
                }

                var hashWrapper = this.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling;
                // console.log('prevprevprevprev : ', hashWrapper);

                // textual hash
                var hash = CryptoJS.SHA1(textarea.value.trim()).toString().toUpperCase();
                hashWrapper.innerText = separatorEveryNChar(hash, ' ', 4) + ' ' + i18next.t('tools.qrcode.hashParenthesis');
                
                // visual hash
                var hashVisualCanvas = this.previousElementSibling.previousElementSibling.previousElementSibling;
                visualHash(hashVisualCanvas, hash);
                hashVisualCanvas.style.display = 'block';
            });

            document.getElementById("cancelButtonQRC" + (i + 1))
            .addEventListener('click', function(e) {
                var hashVisualCanvas = this.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling;
                var textarea = this.previousElementSibling.previousElementSibling.previousElementSibling;
                var errorHash = this.previousElementSibling.previousElementSibling;
                var hashWrapper = this.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling;
                
                hashVisualCanvas.style.display = 'none';
                errorHash.style.display = 'none';
                newError.style.display = 'none';
                hashWrapper.innerText = '';
                textarea.value = '';
            });
        }

        // validate global hash of all QR Codes
        // ------------------------------------
        if(nb > 1) { // display this global hash check only if more than one QR Code.
            var newValidateAll = document.createElement('button');
            var newErrorValidateAllHash = document.createElement('p');
            var newValidateAllHash = document.createElement('p');
            var newValidateAllVisualHash = document.createElement('canvas');
            var labelConcatenatedContent = document.createElement('label');
            var concatenatedContent = document.createElement('textarea');
            var copyConcatenatedContent = document.createElement('button');

            newValidateAllVisualHash.setAttribute("class", "mg5");
            newValidateAllVisualHash.setAttribute("title", i18next.t('tools.qrcode.readQrCode.globalVisualHash'));
            newValidateAllVisualHash.setAttribute("id", 'newValidateAllVisualHash');
            newValidateAllVisualHash.setAttribute("id", 'newValidateAllVisualHash');

            newValidateAll.setAttribute("class", "btn btn-primary btn__qrc--receiver");
            newValidateAll.setAttribute("id", "btnCheckAllQRCodesToRead");
            newValidateAll.innerText = i18next.t('tools.qrcode.readQrCode.globalReconstitutionBtn');
            newErrorValidateAllHash.setAttribute("id", "newErrorValidateAllHash");
            
            labelConcatenatedContent.setAttribute("class", "control-label");
            labelConcatenatedContent.innerText = i18next.t('tools.qrcode.readQrCode.globalReconstitutionField');
            concatenatedContent.setAttribute("class", "form-control");
            concatenatedContent.setAttribute("rows", "10");
            concatenatedContent.setAttribute("id", "concatenatedContent");
            
            QRCReceiverWrapper.appendChild(newValidateAll);
            QRCReceiverWrapper.appendChild(newErrorValidateAllHash);
            QRCReceiverWrapper.appendChild(newValidateAllHash);
            QRCReceiverWrapper.appendChild(newValidateAllVisualHash);
            QRCReceiverWrapper.appendChild(labelConcatenatedContent);
            QRCReceiverWrapper.appendChild(concatenatedContent);

            setDisabled('btnNumberOfQRCodesToRead');
            newValidateAllVisualHash.style.display = 'none';
            labelConcatenatedContent.style.display = 'none';
            concatenatedContent.style.display = 'none';

            // validate at the end to display full hash of concatenated QR Codes
            document.getElementById("btnCheckAllQRCodesToRead").addEventListener('click', function(e) {
                var newErrorValidateAllHash = document.getElementById("newErrorValidateAllHash")
                newErrorValidateAllHash.innerHTML = '';

                // QR Codes empty
                if(concatenatedQRCodesHash() === false) {
                    const context = newValidateAllVisualHash.getContext('2d');
                    context.clearRect(0, 0, newValidateAllVisualHash.width, newValidateAllVisualHash.height);
                    newValidateAllHash.innerText = '';
                    newErrorValidateAllHash.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.QRCodesEmpty') + '</p>'
                    return;
                }

                // textual hash
                newValidateAllHash.innerText = i18next.t('tools.qrcode.finalGlobalHashVerificationText') + separatorEveryNChar(concatenatedQRCodesHash(), ' ', 4);

                // visual global hash
                visualHash(newValidateAllVisualHash, concatenatedQRCodesHash(), 256, 256);
                ; 

                // insert concatenated content in textarea
                 var concatenatedQRCodes = '';
                for (var i = 0; i < nb; i++) {
                    concatenatedQRCodes += document.getElementById("QRC" + (i + 1)).value.trim();
                }

                // display visual hash and concatenated textarea and feed it
                newValidateAllVisualHash.style.display = 'block';
                labelConcatenatedContent.style.display = 'block';
                concatenatedContent.style.display = 'block';
                concatenatedContent.value = concatenatedQRCodes;
            });

            // concatenate all QR Codes and calculate hash
            function concatenatedQRCodesHash() {
                var noEmptyBlock = true;
                var concatenatedQRCodes = '';
                for (var i = 0; i < nb; i++) {
                    if(document.getElementById("QRC" + (i + 1)).value == false) {
                        noEmptyBlock = false;
                    }
                    concatenatedQRCodes += document.getElementById("QRC" + (i + 1)).value.trim();
                }
                var concatenatedQRCodesHash = CryptoJS.SHA1(concatenatedQRCodes).toString().toUpperCase();

                if (noEmptyBlock === true) {
                    return concatenatedQRCodesHash;
                }
                return false;
            }
        }
    }
})(visualHash);