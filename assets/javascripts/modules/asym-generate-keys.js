// ******************
// ASYMMETRIC CYPHER
// ******************

// **************
// GENERATE KEYS
// **************

/*------------------
 Show Password asym.
-------------------*/

(function($) {

    "use strict";

    $("#show_password").click(function() {
      if ($('#show_password').hasClass('bi-eye-fill')) {
        $('#asym_create_form_password').attr('type', 'text');
        $('#show_password').removeClass('bi-eye-fill').addClass('bi-eye-slash-fill');
      }
      else {
        $('#asym_create_form_password').attr('type', 'password');
        $('#show_password').removeClass('bi-eye-slash-fill').addClass('bi-eye-fill');
      }
    });

    $("#show_password_asym").click(function() {
      if ($('#show_password_asym').hasClass('bi-eye-fill')) {
        $('#asym_form_password').attr('type', 'text');
        $('#show_password_asym').removeClass('bi-eye-fill').addClass('bi-eye-slash-fill');
      }
      else {
        $('#asym_form_password').attr('type', 'password');
        $('#show_password_asym').removeClass('bi-eye-slash-fill').addClass('bi-eye-fill');
      }
    });

})(window.jQuery);


(function() {

    "use strict";
    
    var generateKeys = document.getElementById('generateKeys');
    if(generateKeys){
        generateKeys.addEventListener('click', function(e){
            var asymKeyName = document.getElementById('asymKeyName').value;
            var asymKeyEmail = document.getElementById('asymKeyEmail').value;

            var errorGenerateKeys = document.getElementById("errorGenerateKeys");
            errorGenerateKeys.innerHTML = '';
            
            var asym_create_form_password = document.getElementById('asym_create_form_password').value;
            if('' == asym_create_form_password || asym_create_form_password.trim().length < 16) {                           
                document.getElementById("errorGenerateKeys").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.asymPasswordMinLength') + '</p>'
                return;
            }
            
            var errorAsymCypherEncrypt = document.getElementById("errorAsymCypherEncrypt");
            errorAsymCypherEncrypt.innerHTML = '';

            // get validity
            var keyExpirationTime = document.getElementById('asymKeyValidity').value;

            (async () => {
                const { privateKey, publicKey, revocationCertificate } = await openpgp.generateKey({
                    type: 'ecc', // type of the key, defaults to ECC
                    curve: 'curve25519', // ECC curve name, defaults to curve25519
                    userIDs: [{ name: asymKeyName.trim(), email: asymKeyEmail.trim() }],
                    passphrase: asym_create_form_password.trim(),
                    keyExpirationTime: keyExpirationTime,
                    format: 'armored'
                });

                const publicKeyCheck = await openpgp.readKey({ armoredKey: publicKey });
                
                // -----BEGIN PGP PRIVATE KEY BLOCK-----
                addText(privateKey.trim(), "privateKeyGenerated");
                removeDisabled('privateKeyGenerated');

                // -----BEGIN PGP PUBLIC KEY BLOCK-----
                addText(publicKey.trim(), "publicKeyGenerated");
                removeDisabled('publicKeyGenerated');
                
                // -----BEGIN PGP PUBLIC KEY BLOCK-----
                // Comment: This is a revocation certificate
                addText(revocationCertificate.trim(), "revocationCertificate");
                removeDisabled('revocationCertificate');

                addText(publicKeyCheck.keyPacket.keyID.toHex().toUpperCase(), "pubKeyID");
                removeDisabled('pubKeyID');

                setDisabled('generateKeys');
                removeDisabled('cancelGenerateKeys');
            })();
        });
    }

    // cancel
    var cancelGenerateKeys = document.getElementById('cancelGenerateKeys');
    if(cancelGenerateKeys){
        cancelGenerateKeys.addEventListener('click', function(e){
            e.preventDefault();
            var errorGenerateKeys = document.getElementById("errorGenerateKeys");
            errorGenerateKeys.innerHTML = '';

            const asym_create_form_password = document.getElementById('asym_create_form_password');
                  asym_create_form_password.value = '';

            const privateKeyGenerated = document.getElementById("privateKeyGenerated");
                  privateKeyGenerated.value = '';

            const publicKeyGenerated = document.getElementById("publicKeyGenerated");
                  publicKeyGenerated.value = '';

            const revocationCertificate = document.getElementById("revocationCertificate");
                  revocationCertificate.value = '';

            const pubKeyID = document.getElementById("pubKeyID");
                  pubKeyID.value = '';

            setDisabled('privateKeyGenerated');
            setDisabled('publicKeyGenerated');
            setDisabled('revocationCertificate');
            
            removeDisabled('generateKeys');
            setDisabled('cancelGenerateKeys');
        })
    }
})();