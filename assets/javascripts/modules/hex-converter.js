// **************
// HEX CONVERTER
// **************


// TOOLS (QR CODE)
// ***************

(function() {

    "use strict";

    // CONVERT TO HEX
    // **************
    // button to trigger the conversion to Hex
    var buttonToConvertToHex = document.getElementById('buttonToConvertToHex');
    if(buttonToConvertToHex){
        buttonToConvertToHex.addEventListener('click', function(e){
            var errorConvertToHex = document.getElementById("errorConvertToHex");
            errorConvertToHex.innerHTML = '';

            var strToConvertToHex = document.getElementById("strToConvertToHex").value;
            if('' == strToConvertToHex) {                           
                document.getElementById("errorConvertToHex").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textToHexaEmpty') + '</p>'
                return;
            }
            convertFromStrToHex();
        });
    }

    // convert the input entered by the user to hex string.
    function convertFromStrToHex() {
        function convertToHex(strToConvertToHex) {
            // clear first
            const strConv = document.getElementById("strConvertedToHex");
            strConv.value = '';           

            var hex = xenStrToHex(strToConvertToHex);

            return hex.toUpperCase();
        }
        var strToConvertToHex = document.getElementById("strToConvertToHex").value;
        var strConvertedToHex = convertToHex(strToConvertToHex);
        addText(strConvertedToHex, 'strConvertedToHex');

        setDisabled('strToConvertToHex');
        removeDisabled('strConvertedToHex');
        
        setDisabled('buttonToConvertToHex');
        removeDisabled('cancelConvertToHex');
    }

    // cancel
    var cancelConvertToHex = document.getElementById('cancelConvertToHex');
    if(cancelConvertToHex){
        cancelConvertToHex.addEventListener('click', function(e){
            e.preventDefault();
            var errorConvertToHex = document.getElementById("errorConvertToHex");
            errorConvertToHex.innerHTML = '';

            const nodeStrToConvertToUTF8 = document.getElementById("strToConvertToHex");
                  nodeStrToConvertToUTF8.value = '';

            const nodeStrConvertedToUTF8 = document.getElementById("strConvertedToHex");
                  nodeStrConvertedToUTF8.value = '';

            removeDisabled('strToConvertToHex');
            setDisabled('strConvertedToHex');
            
            removeDisabled('buttonToConvertToHex');
            setDisabled('cancelConvertToHex');
        })
    }


    // CONVERT TO STRING (UTF8)
    // ************************
    // button to trigger the conversion to string UTF8
    var buttonToConvertToUTF8 = document.getElementById('buttonToConvertToUTF8');
    if(buttonToConvertToUTF8){
        buttonToConvertToUTF8.addEventListener('click', function(e){
            var errorConvertToUTF8 = document.getElementById("errorConvertToUTF8");
            errorConvertToUTF8.innerHTML = '';

            var strToConvertToUTF8 = document.getElementById("strToConvertToUTF8").value;
            if('' == strToConvertToUTF8) {                           
                document.getElementById("errorConvertToUTF8").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.hexaToUTF8Empty') + '</p>'
                return;
            }

            if(!isHex(strToConvertToUTF8)) {
                document.getElementById("errorConvertToUTF8").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.notInHexa') + '</p>'
                return;
            }
            convertFromHexToStr();
        });
    }

    // convert the input entered by the user to hex string.
    function convertFromHexToStr() {
        function convertToUTF8(strToConvertToUTF8) {

            // clear first
            const strConv = document.getElementById("strConvertedToUTF8");
            strConv.value = '';
           
            var str = xenHexToStr(strToConvertToUTF8);

            return str;
        }
        var strToConvertToUTF8 = document.getElementById("strToConvertToUTF8").value;
        var strConvertedToUTF8 = convertToUTF8(strToConvertToUTF8);
        addText(strConvertedToUTF8, 'strConvertedToUTF8');

        setDisabled('strToConvertToUTF8');
        removeDisabled('strConvertedToUTF8');
        
        setDisabled('buttonToConvertToUTF8');
        removeDisabled('cancelConvertToUTF8');
    }

    // cancel
    var cancelConvertToUTF8 = document.getElementById('cancelConvertToUTF8');
    if(cancelConvertToUTF8){
        cancelConvertToUTF8.addEventListener('click', function(e){
            e.preventDefault();
            var errorConvertToUTF8 = document.getElementById("errorConvertToUTF8");
            errorConvertToUTF8.innerHTML = '';

            const nodeStrToConvertToUTF8 = document.getElementById("strToConvertToUTF8");
                  nodeStrToConvertToUTF8.value = '';

            const nodeStrConvertedToUTF8 = document.getElementById("strConvertedToUTF8");
                  nodeStrConvertedToUTF8.value = '';

            removeDisabled('buttonToConvertToUTF8');
            removeDisabled('strToConvertToUTF8');
            setDisabled('strConvertedToUTF8');
            setDisabled('cancelConvertToUTF8');
        })
    }
})();


// OTP
// *****

(function() {

    "use strict";

    // CONVERT TO HEX OTP
    // ******************
    // button to trigger the conversion to Hex
    var buttonToConvertOtpToHex = document.getElementById('buttonToConvertOtpToHex');
    if(buttonToConvertOtpToHex){
        buttonToConvertOtpToHex.addEventListener('click', function(e){
            var errorConvertOtpToHex = document.getElementById("errorConvertOtpToHex");
            errorConvertOtpToHex.innerHTML = '';

            var strToConvertOtpToHex = document.getElementById("strToConvertOtpToHex").value;
            if('' == strToConvertOtpToHex) {                           
                document.getElementById("errorConvertOtpToHex").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.convertTextToHexa') + '</p>'
                return;
            }
            convertFromStrToHexOTP();
        });
    }

    // convert the input entered by the user to hex string.
    function convertFromStrToHexOTP() {
        function convertToHexOTP(strToConvertOtpToHex) {
            // clear first
            const strConv = document.getElementById("strConvertedOtpToHex");
            strConv.value = '';

            var hex = xenStrToHex(strToConvertOtpToHex);

            return hex.toUpperCase();
        }

        var strToConvertOtpToHex = document.getElementById("strToConvertOtpToHex").value;
        var strConvertedOtpToHex = convertToHexOTP(strToConvertOtpToHex);

        addText(strConvertedOtpToHex, 'strConvertedOtpToHex');
        removeDisabled('strConvertedOtpToHex');
        setDisabled('buttonToConvertOtpToHex');
        setDisabled('strToConvertOtpToHex');
    }

    // cancel convert otp to hex
    var cancelConvertOtpToHex = document.getElementById('cancelConvertOtpToHex');
    if(cancelConvertOtpToHex){
        cancelConvertOtpToHex.addEventListener('click', function(e){
            e.preventDefault();
            var errorConvertOtpToHex = document.getElementById("errorConvertOtpToHex");
            errorConvertOtpToHex.innerHTML = '';

            const nodeStrToConvertOtpToUTF8 = document.getElementById("strToConvertOtpToHex");
                  nodeStrToConvertOtpToUTF8.value = '';

            const nodeStrConvertedOtpToUTF8 = document.getElementById("strConvertedOtpToHex");
                  nodeStrConvertedOtpToUTF8.value = '';

            removeDisabled('buttonToConvertOtpToHex');
            removeDisabled('strToConvertOtpToHex');
            setDisabled('strConvertedOtpToHex');
        })
    }


    // CONVERT TO STRING (UTF8) OTP
    // ****************************
    // button to trigger the conversion to string UTF8
    var buttonToConvertOtpToUTF8 = document.getElementById('buttonToConvertOtpToUTF8');
    if(buttonToConvertOtpToUTF8){
        buttonToConvertOtpToUTF8.addEventListener('click', function(e){
            var errorConvertOtpToUTF8 = document.getElementById("errorConvertOtpToUTF8");
            errorConvertOtpToUTF8.innerHTML = '';

            var strToConvertOtpToUTF8 = document.getElementById("strToConvertOtpToUTF8").value;
            if('' == strToConvertOtpToUTF8) {                           
                document.getElementById("errorConvertOtpToUTF8").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.hexaToTextEmpty') + '</p>'
                
                return;
            }
            convertFromHexToStrOtp();
        });
    }

    // convert the input entered by the user to hex string.
    function convertFromHexToStrOtp() {
        var strToConvertOtpToUTF8 = document.getElementById("strToConvertOtpToUTF8").value;
        if(!isHex(strToConvertOtpToUTF8)) {
            document.getElementById("errorConvertOtpToUTF8").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.notInHexa') + '</p>'
            
            return;
        }

        function convertToUTF8Otp(strToConvertOtpToUTF8) {

            // clear first
            const strConv = document.getElementById("strConvertedOtpToUTF8");
            strConv.value = '';

            var str = xenHexToStr(strToConvertOtpToUTF8);

            return str;
        }

        var strConvertedOtpToUTF8 = convertToUTF8Otp(strToConvertOtpToUTF8);
        addText(strConvertedOtpToUTF8, 'strConvertedOtpToUTF8');
        removeDisabled('strConvertedOtpToUTF8');
        setDisabled('buttonToConvertOtpToUTF8');
        setDisabled('strToConvertOtpToUTF8');
    }

    // cancel convert otp to utf8
    var cancelConvertOtpToUTF8 = document.getElementById('cancelConvertOtpToUTF8');
    if(cancelConvertOtpToUTF8){
        cancelConvertOtpToUTF8.addEventListener('click', function(e){
            e.preventDefault();
            var errorConvertOtpToUTF8 = document.getElementById("errorConvertOtpToUTF8");
            errorConvertOtpToUTF8.innerHTML = '';

            const nodeStrToConvertOtpToUTF8 = document.getElementById("strToConvertOtpToUTF8");
                  nodeStrToConvertOtpToUTF8.value = '';

            const nodeStrConvertedOtpToUTF8 = document.getElementById("strConvertedOtpToUTF8");
                  nodeStrConvertedOtpToUTF8.value = '';

            removeDisabled('buttonToConvertOtpToUTF8');
            removeDisabled('strToConvertOtpToUTF8');
            setDisabled('strConvertedOtpToUTF8');
        })
    }
})();