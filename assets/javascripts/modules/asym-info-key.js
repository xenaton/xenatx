// ************************
// ASYMMETRIC CYPHER (AES)
// ************************

// **************
// GENERATE KEYS
// **************

(function() {

    "use strict";
    
    var btnInfoKeyPub = document.getElementById('btnInfoKeyPub');

    if(btnInfoKeyPub){
        btnInfoKeyPub.addEventListener('click', function(e){
            e.preventDefault();
            
            var errorInfoKeyPub = document.getElementById('errorInfoKeyPub');
            errorInfoKeyPub.innerHTML = '';
            
            var inputInfoKeyPub = document.getElementById('inputInfoKeyPub').value;
            if('' == inputInfoKeyPub) {                           
                document.getElementById("errorInfoKeyPub").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.publicKeyInfoEmpty') + '</p>'
                return;
            }

            (async () => {

                function buf2hex(buffer) { // buffer is an ArrayBuffer
                    return [...new Uint8Array(buffer)]
                        .map(x => x.toString(16).padStart(2, '0'))
                        .join('');
                }

                function separatorEveryNChar(str, separator, nChar) {
                    var chain = '';
                    var resultTab = [];
                    for (var i=0; i < str.length; i++) {
                        chain += str[i];

                        if(chain.length % nChar === 0) {
                            var temp = chain.slice(chain.length - nChar);
                            resultTab.push(temp);
                        }
                    }
                    return resultTab.join(separator);
                }
                
                try {
                    const pubKeyCheck = await openpgp.readKey({ armoredKey: inputInfoKeyPub });

                    var expirationDate = await pubKeyCheck.getExpirationTime();
                    if(expirationDate == 'Infinity'){
                        expirationDate = "Jamais";
                    }

                    var asymKeyName = pubKeyCheck.users[0].userID.name;
                    if(asymKeyName == 0){
                        asymKeyName = "Aucun nom";
                    }

                    var asymKeyEmail = pubKeyCheck.users[0].userID.email;
                    if(asymKeyEmail == 0){
                        asymKeyEmail = "Aucun email";
                    }

                    var fingerprintView = buf2hex(pubKeyCheck.keyPacket.fingerprint);

                    fingerprintView = separatorEveryNChar(fingerprintView, '  ', 4);
                    var outputInfoKeyPub = document.getElementById('outputInfoKeyPub');

                    // check wether key is revoked or not
                    var keyRevocationStatus = i18next.t('various.keyRevocationStatusNo');
                    if(pubKeyCheck.revocationSignatures.length !== 0) {
                        keyRevocationStatus = i18next.t('various.keyRevocationStatusYes');
                    }

                    outputInfoKeyPub.value =  
                        "Révocation : " + keyRevocationStatus
                        + "\n" + "\n" + "Propriétaire : " + asymKeyName + ' < ' + asymKeyEmail + ' >'
                        + "\n" + "Créée le : " + pubKeyCheck.keyPacket.created 
                        + "\n" + "Date d'expiration : " + expirationDate
                        + "\n" + "Empreinte : " + fingerprintView.toUpperCase() 
                        + "\n" + "Identifiant : " + pubKeyCheck.keyPacket.keyID.toHex().toUpperCase()
                        + "\n" + "Version : " + pubKeyCheck.keyPacket.version
                    ;
                    // console.log(pubKeyCheck.revocationSignatures);
                    removeDisabled('outputInfoKeyPub');
                    removeDisabled('cancelInfoKeyPub');

                    setDisabled('inputInfoKeyPub');
                    setDisabled('btnInfoKeyPub');
                } 
                catch (e) {
                    document.getElementById("errorInfoKeyPub").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Informations de clé : ' + e.message + '.</p>'
                    return;
                }

            })();
        });

        // cancel
        var cancelInfoKeyPub = document.getElementById('cancelInfoKeyPub');
        if(cancelInfoKeyPub){
            cancelInfoKeyPub.addEventListener('click', function(e){
                e.preventDefault();
                var errorInfoKeyPub = document.getElementById("errorInfoKeyPub");
                errorInfoKeyPub.innerHTML = '';

                const inputInfoKeyPub = document.getElementById("inputInfoKeyPub");
                    inputInfoKeyPub.value = '';

                const outputInfoKeyPub = document.getElementById("outputInfoKeyPub");
                    outputInfoKeyPub.value = '';

                removeDisabled('crypt');
                removeDisabled('inputInfoKeyPub');
                setDisabled('outputInfoKeyPub');
                
                removeDisabled('btnInfoKeyPub');
                setDisabled('cancelInfoKeyPub');
            })
        }
    }
})();